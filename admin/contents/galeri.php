<?php
	include ('contents/koneksi.php');

	function ShowData($koneksi)
	{
		echo "<div class='galeri'>";
		echo "<h1>Gallery</h1>";
		echo "<hr>";
		echo "<a href='?page=galeri&add' class='btn btn-primary'><span class='fa fa-plus'></span> Tambah Kategori</a>";
		echo "<hr>";
		echo "<table class='table table-striped table-bordered' style='margin-top:10px;'>
		<tr>
		<th>#</th>
		<th>Gambar</th>
		<th>Action</th>
		</tr>
		";
		echo "</div>";
		$i=1;
		$query = mysqli_query($koneksi, "SELECT * FROM galeri");
		while ($data = mysqli_fetch_array($query)) {
			echo "<tr>";
			echo "<td>".$i++."</td>";
			echo "<td>".$data['image']."</td>";
			echo "<td>
			<a class='fa fa-edit' href='?page=galeri&add&id=".$data['id_galeri']."'></a>
			<a class='fa fa-trash' href='?page=galeri&act=hapus&id=".$data['id_galeri']."'></a>
			</td>";
			echo "</tr>";
		}
	}
	function form($koneksi){
		if (isset($_GET['id'])) {
			$query = mysqli_query($koneksi, "SELECT * FROM galeri WHERE id_galeri = '".$_GET['id']."'");
			$data = mysqli_fetch_array($query);
			$id_galeri = "<input type='hidden' name='id' value='".$data['id_galeri']."'>";
			$image = $data['image'];
			$button = "<button class='btn btn-primary btn-md update' type='submit' name='update'>Update</button>";
		} else {
			$id_galeri = '';
			$image = '';
			$button = "<button class='btn btn-primary btn-md insert' type='submit' name='insert'>Insert</button>";
		}
?>	
		<form class="form-insert" method="post" action="?page=galeri">
			<?php 
			if (isset($_GET['id'])){
				echo "<h3>Update Data</h3>";
			}else{
				echo "<h3>Tambah Data</h3>";
			}
				echo "<hr>";
				echo $id_galeri;
			?>
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Image</label>
				<div class="col-sm-6">
					<input type="file" name="image"  value="<?php echo $tag; ?>" required>
				</div>
			</div>
			<?php echo $button; ?>
		</form>
<?php
	}
	function insert($koneksi){
		$query = sprintf("INSERT INTO galeri(image) VALUES ('%s')",$_POST['image']);
		$act = mysqli_query($koneksi, $query);
		if ($act) {
			header('location:index.php?page=galeri');
		} else {
			echo "<div class='alert alert-danger' role='alert'><span class='fa fa-close'></span> Gagal Insert Data!</div>";
		}
	}
	function delete($koneksi){
		$query = sprintf("delete from galeri where id_galeri='%s'",$_GET['id']);
		$act = mysqli_query($koneksi,$query);
		if($act){
			header('location:?page=galeri');
		}else{
			echo "gagal".mysqli_error($koneksi);
		} 
	}
	function update($koneksi){
    	$query = sprintf("UPDATE galeri SET image = '%s' WHERE id_galeri = '%s'",$_POST['image'],$_POST['id']);
		$act = mysqli_query($koneksi,$query) or die(mysqli_error($koneksi));
		if ($act) {
			header('location:?page=galeri');
		}else{
			echo "<br>gagal Update";
		}

}

	if (isset($_GET['add'])) {
		if (isset($_GET['act']) && $_GET['act'] == 'edit') update($koneksi);
		form($koneksi);
	} else {
		ShowData($koneksi);
		if (isset($_GET['act']) && $_GET['act'] == 'hapus') delete($koneksi);
	}
	if (isset($_POST['insert'])) insert($koneksi);
	else if (isset($_POST['update'])) update($koneksi);
?>