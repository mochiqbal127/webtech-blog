<?php
	include ('contents/koneksi.php');

	function ShowData($koneksi)
	{
		echo "<div class='artikel'>";
		echo "<h1>Artikel</h1>";
		echo "<hr>";
		echo "<a href='?page=artikel&add' class='btn btn-primary'><span class='fa fa-plus'></span> Tambah Artikel</a>";
		echo "<hr>";
		echo "<table class='table table-striped table-bordered' style='margin-top:10px;'>
		<tr>
		<th width='3%'><center>#</center></th>
		<th width='15%'><center>ID Kategori</center></th>
		<th width='70%'><center>Judul</center></th>
		<th width='10%'><center>Action</center></th>
		</tr>
		";
		echo "</div>";
		$i=1;
		$query = mysqli_query($koneksi, "SELECT * FROM artikel JOIN kategori on artikel.id_kategori = kategori.id_kategori");
		while ($data = mysqli_fetch_array($query)) {
			echo "<tr>";
			echo "<td><center>".$i++."</center></td>";
			echo "<td>".$data['id_kategori']."</td>";
			echo "<td>".$data['judul']."</td>";
			echo "<td><center>
			<a class='fa fa-edit' href='?page=artikel&add=edit&id=".$data['id_artikel']."'></a>
			<a class='fa fa-trash' href='?page=artikel&act=hapus&id=".$data['id_artikel']."'></a>
			</td>";
			echo "</center></tr>";
		}
	}
	function form($koneksi){
		if (isset($_GET['id'])) {
			$query = mysqli_query($koneksi, "SELECT * FROM artikel WHERE id_artikel = '".$_GET['id']."'");
			$data = mysqli_fetch_array($query);
			$id_artikel ="<input type='hidden' name='id' value='".$data['id_artikel']."'>";
			$id_kategori = $data['id_kategori'];
			$judul = $data['judul'];
			$isi = $data['isi'];
			$image = $data['image'];
			$id_user = $_SESSION['id_user'];
			$button = "<button class='btn btn-primary btn-md update' type='submit' name='update'>Update</button>";
		} else {
			$id_artikel = '';
			$id_kategori = '';
			$judul = '';
			$isi = '';
			$image = '';
			$id_user = $_SESSION['id_user'];
			$button = "<button class='btn btn-primary btn-md insert' type='submit' name='insert'>Insert</button>";
		}
?>	
		<form class="form-insert" method="post" action="?page=artikel">
			<?php 
			if (isset($_GET['id'])){
				echo "<h3>Update Data</h3>";
			}else{
				echo "<h3>Tambah Data</h3>";
			}
				echo "<hr>";
				echo $id_artikel;
			?>
			<div class="form-group">
				<label class="col-md-2 col-form-label">Kategori</label>
				<div class="col-sm-6">
					<select class="form-control" name="id_kategori" id="id_kategori">
						<option value="">Pilih Kategori</option>
						<?php 
						$kate = sprintf("SELECT * FROM kategori");
						$query = mysqli_query($koneksi,$kate);
						while ($data = mysqli_fetch_array($query)){
						$nama = $data['nama_kategori'];
						$id = $data['id_kategori'];						
						?>
						<option value="<?php echo $id ?>"><?php echo $nama; ?></option>
					<?php } ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-form-label">Judul</label>
				<div class="col-sm-6">
					<input type="text" name="judul" class="form-control" value="<?php echo $judul; ?>" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-form-label">Isi Artikel</label>
				<div class="col-sm-6">
					<textarea id="isi" name="isi" class="form-control" rows="30"><?php echo $isi; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-form-label">Image</label>
				<div class="col-sm-6">
					<input type="file" name="image" value="<?php echo $image; ?>" required>
				</div>
			</div>
			<input type="hidden" name="id_user" value="<?php echo $id_user; ?>">
			<?php echo $button; ?>
		</form>
<?php
	}
	function insert($koneksi){
		$query = sprintf("INSERT INTO artikel(id_artikel, id_kategori, judul, isi, image, id_user) VALUES ('%s', '%s', '%s', '%s', '%s', '%s')", $_POST['id'], $_POST['id_kategori'], $_POST['judul'], $_POST['isi'], $_POST['image'], $_POST['id_user']);
		$act = mysqli_query($koneksi, $query);
		if ($act) {
			header('location:?page=artikel');
		} else {
			echo "<div class='alert alert-danger' role='alert'><span class='fa fa-close'></span> Gagal Insert Data!</div>";
		}
	}
	function delete($koneksi){
		$query = sprintf("DELETE FROM artikel WHERE id_artikel='%s'",$_GET['id']);
		$act = mysqli_query($koneksi,$query);
		if($act){
			header('location:?page=artikel');
		}else{
			echo "gagal".mysqli_error($koneksi);
		}
	}
	function update($koneksi){
		$query = sprintf("UPDATE artikel SET id_kategori='%s', judul ='%s', isi='%s',image='%s',id_user='%s' WHERE id_artikel = '%s'",$_POST['id_kategori'],$_POST['judul'],$_POST['isi'],$_POST['image'],$_POST['id_user'],$_POST['id']);
		$act = mysqli_query($koneksi,$query) or die(mysqli_error($koneksi));
		if ($act) {
			header('location:?page=artikel');
		}else{
			echo "<br>gagal Update";
		}
	}

	if (isset($_GET['add'])) {
		form($koneksi);
	} else {
		ShowData($koneksi);
		if (isset($_GET['act']) && $_GET['act'] == 'hapus') delete($koneksi);
	}
	if (isset($_POST['insert'])) insert($koneksi);
	else if (isset($_POST['update'])) update($koneksi);
?>
