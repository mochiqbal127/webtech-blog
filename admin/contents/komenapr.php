<?php
	include ('contents/koneksi.php');

	function ShowData($koneksi)
	{
		echo "<div class='komentar'>";
		echo "<h1>Komentar yang Belum DiSetujui</h1>";
		echo "<hr>";
		echo "<table class='table table-striped table-bordered' style='margin-top:10px;'>
		<tr>
		<th>#</th>
		<th>Id Artikel</th>
		<th>Email</th>
		<th>Nama</th>
		<th>Komentar</th>
		<th>Action</th>
		</tr>
		";
		echo "</div>";
		$i=1;
		$query = mysqli_query($koneksi, "SELECT * FROM komentar");
		while ($data = mysqli_fetch_array($query)) {
			echo "<tr>";
			echo "<td>".$i++."</td>";
			echo "<td>".$data['id_artikel']."</td>";
			echo "<td>".$data['email']."</td>";
			echo "<td>".$data['nama']."</td>";
			echo "<td>".$data['komentar']."</td>";			
			echo "<td class='act'>			
			<a class='fa fa-edit' href='?page=komen&apr&id=".$data['id_komentar']."'></a>
			<a class='fa fa-trash' href='?page=komen&act=hapus&id=".$data['id_komentar']."'></a>
			</td>";
			echo "</tr>";
		}
	}
	function ShowDataApr($koneksi)
	{
		echo "<div class='komentar'>";
		echo "<h1>Komentar</h1>";
		echo "<hr>";
		echo "<a href='?page=komen&add' class='btn btn-primary'><span class='fa fa-plus'></span> Setujui Komentar</a>";
		echo "<hr>";
		echo "<table class='table table-striped table-bordered' style='margin-top:10px;'>
		<tr>
		<th>#</th>
		<th>Id Artikel</th>
		<th>Email</th>
		<th>Nama</th>
		<th>Komentar</th>
		<th>Action</th>
		</tr>
		";
		echo "</div>";
		$i=1;
		$query = mysqli_query($koneksi, "SELECT * FROM komentar_apr");
		while ($data = mysqli_fetch_array($query)) {
			echo "<tr>";
			echo "<td>".$i++."</td>";
			echo "<td>".$data['id_artikel']."</td>";
			echo "<td>".$data['email']."</td>";
			echo "<td>".$data['nama']."</td>";
			echo "<td>".$data['komentar']."</td>";			
			echo "<td class='act'>			
			<a class='fa fa-trash' href='?page=komen&act=hapus&id=".$data['id_komentar']."'></a>
			</td>";
			echo "</tr>";
		}
	}
		
?>	
		
<?php	
	function delete_apr($koneksi){
		$query = sprintf("delete from komentar_apr where id_komentar='%s'",$_GET['id']);
		$act = mysqli_query($koneksi,$query);
		if($act){
			header('location:?page=komen');
		}else{
			echo "gagal".mysqli_error($koneksi);
		} 
	}
	function delete($koneksi){
		$query = sprintf("delete from komentar where id_komentar='%s'",$_GET['id']);
		$act = mysqli_query($koneksi,$query);
		if($act){
			header('location:?page=komen&add');
		}else{
			echo "gagal".mysqli_error($koneksi);
		} 
	}
	function input($koneksi){
		if (isset($_GET['id'])) {
			$query = mysqli_query($koneksi, "SELECT * FROM komentar WHERE id_komentar = '".$_GET['id']."'");
			$data = mysqli_fetch_array($query);
			$id_komentar = $data['id_komentar'];
			$id_artikel = $data['id_artikel'];			
			$email = $data['email'];
			$nama = $data['nama'];
			$komentar = $data['komentar'];
			$created_at = $data['created_at'];
			$updated_at = $data['updated_at'];
			}
		$query = sprintf("insert into komentar_apr values('%s','%s','%s','%s','%s','%s','%s')",$id_komentar,$id_artikel,$email,$nama,$komentar,$created_at,$updated_at);
		$act = mysqli_query($koneksi,$query);
		if($act){
			header('location:?page=komen');
		}else{
			echo "gagal".mysqli_error($koneksi);
		}
	}

if (isset($_GET['add'])) {
	ShowData($koneksi);
	if (isset($_GET['act']) && $_GET['act'] == 'apr') input($koneksi);
	elseif (isset($_GET['act']) && $_GET['act'] == 'hapus') delete($koneksi);
} else {
	ShowDataApr($koneksi);
	if (isset($_GET['act']) && $_GET['act'] == 'hapus') delete_apr($koneksi);
}
?>