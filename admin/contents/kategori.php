<?php
	include ('contents/koneksi.php');

	function ShowData($koneksi)
	{
		echo "<div class='Kategori'>";
		echo "<h1>Categories</h1>";
		echo "<hr>";
		echo "<a href='?page=kategori&add' class='btn btn-primary'><span class='fa fa-plus'></span> Tambah Kategori</a>";
		echo "<hr>";
		echo "<table class='table table-striped table-bordered' style='margin-top:10px;'>
		<tr>
		<th>#</th>
		<th>Name Kategori</th>
		<th>Tag</th>
		<th>Action</th>
		</tr>
		";
		echo "</div>";
		$i=1;
		$query = mysqli_query($koneksi, "SELECT * FROM kategori");
		while ($data = mysqli_fetch_array($query)) {
			echo "<tr>";
			echo "<td>".$i++."</td>";
			echo "<td>".$data['nama_kategori']."</td>";
			echo "<td>".$data['tag']."</td>";			
			echo "<td>
			<a class='fa fa-edit' href='?page=kategori&add&id=".$data['id_kategori']."'></a>
			<a class='fa fa-trash' href='?page=kategori&act=hapus&id=".$data['id_kategori']."'></a>
			</td>";
			echo "</tr>";
		}
	}
	function form($koneksi){
		if (isset($_GET['id'])) {
			$query = mysqli_query($koneksi, "SELECT * FROM kategori WHERE id_kategori = '".$_GET['id']."'");
			$data = mysqli_fetch_array($query);
			$id_kategori = "<input type='hidden' name='id' value='".$data['id_kategori']."'>";
			$nama_kategori = $data['nama_kategori'];
			$tag = $data['tag'];
			$image = $data['image'];
			$button = "<button class='btn btn-primary btn-md update' type='submit' name='update'>Update</button>";
		} else {
			$id_kategori = '';
			$nama_kategori = '';
			$tag = '';
			$image = '';
			$button = "<button class='btn btn-primary btn-md insert' type='submit' name='insert'>Insert</button>";
		}
?>	
		<form class="form-insert" method="post" action="?page=kategori">
			<?php 
			if (isset($_GET['id'])){
				echo "<h3>Update Data</h3>";
			}else{
				echo "<h3>Tambah Data</h3>";
			}
				echo "<hr>";
				echo $id_kategori;
			?>
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Nama Kategori</label>
				<div class="col-sm-6">
					<input type="text" name="nama_kategori" class="form-control" value="<?php echo $nama_kategori; ?>" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Tag</label>
				<div class="col-sm-6">
					<input type="text" name="tag" class="form-control" value="<?php echo $tag; ?>" required>
				</div>
			</div>
			<div class="form-group row">
				<label class="col-md-2 col-form-label">Image</label>
				<div class="col-sm-6">
					<input type="file" name="image"  value="<?php echo $tag; ?>" required>
				</div>
			</div>
			<?php echo $button; ?>
		</form>
<?php
	}
	function insert($koneksi){
		$query = sprintf("INSERT INTO kategori(nama_kategori, tag, image) VALUES ('%s', '%s', '%s')",$_POST['nama_kategori'], $_POST['tag'], $_POST['image']);
		$act = mysqli_query($koneksi, $query);
		if ($act) {
			header('location:?page=kategori');
		} else {
			echo "<div class='alert alert-danger' role='alert'><span class='fa fa-close'></span> Gagal Insert Data!</div>";
		}
	}
	function delete($koneksi){
		$query = sprintf("delete from kategori where id_kategori='%s'",$_GET['id']);
		$act = mysqli_query($koneksi,$query);
		if($act){
			header('location:?page=kategori');
		}else{
			echo "gagal".mysqli_error($koneksi);
		} 
	}
	function update($koneksi){
    	$query = sprintf("UPDATE kategori SET nama_kategori='%s', tag = '%s', image = '%s' WHERE id_kategori = '%s'",$_POST['nama_kategori'],$_POST['tag'],$_POST['image'],$_POST['id']);
		$act = mysqli_query($koneksi,$query) or die(mysqli_error($koneksi));
		if ($act) {
			header('location:?page=kategori');
		}else{
			echo "<br>gagal Update";
		}

}

	if (isset($_GET['add'])) {
		if (isset($_GET['act']) && $_GET['act'] == 'edit') update($koneksi);
		form($koneksi);
	} else {
		ShowData($koneksi);
		if (isset($_GET['act']) && $_GET['act'] == 'hapus') delete($koneksi);
	}
	if (isset($_POST['insert'])) insert($koneksi);
	else if (isset($_POST['update'])) update($koneksi);
?>