<?php
	include ('contents/koneksi.php');

	function ShowData($koneksi)
	{
		echo "<div class='pages'>";
		echo "<h1>Pages</h1>";
		echo "<hr>";
		echo "<a href='?page=pages&add' class='btn btn-primary'><span class='fa fa-plus'></span> Tambah Pages</a>";
		echo "<hr>";
		echo "<table class='table table-striped table-bordered' style='margin-top:10px;'>
		<tr>
		<th width='3%'>#</th>
		<th width='20%'><center>Nama Halaman</center></th>
		<th width='67%'><center>Judul</center></th>		
		<th width='10%'>Action</th>
		</tr>
		";
		echo "</div>";
		$i=1;
		$query = mysqli_query($koneksi, "SELECT * FROM pages");
		while ($data = mysqli_fetch_array($query)) {
			echo "<tr>";
			echo "<td>".$i++."</td>";
			echo "<td>".$data['name_page']."</td>";
			echo "<td>".$data['judul']."</td>";		
			echo "<td>
			<a class='fa fa-edit' href='?page=pages&add&id=".$data['id_page']."'></a>
			<a class='fa fa-trash' href='?page=pages&act=hapus&id=".$data['id_page']."'></a>
			</td>";
			echo "</tr>";
		}
	}
	function form($koneksi){
		if (isset($_GET['id'])) {
			$query = mysqli_query($koneksi, "SELECT * FROM pages WHERE id_page = '".$_GET['id']."'");			
			$data = mysqli_fetch_array($query);
			$id_page = "<input type='hidden' name='id' value='".$data['id_page']."'>";
			$name_page = $data['name_page'];
			$judul = $data['judul'];
			$isi = $data['isi'];
			$image = $data['image'];
			$button = "<button class='btn btn-primary btn-md update' type='submit' name='update'>Update</button>";
		} else {			
			$id_page = '';
			$name_page = '';
			$judul = '';
			$isi = '';
			$image = '';
			$button = "<button class='btn btn-primary btn-md insert' type='submit' name='insert'>Insert</button>";
		}
?>		
		<form class="form-insert" method="POST" action="?page=pages">
			<?php 
			if (isset($_GET['id'])){
				echo "<h3>Update Data</h3>";
			}else{
				echo "<h3>Tambah Data</h3>";
			}
				echo "<hr>";
				echo $id_page;
			?>
			<div class="form-group">
				<label class="col-md-2 col-form-label">Name Page</label>
				<div class="col-sm-6">
					<input type="text" name="name_page" class="form-control" value="<?php echo $name_page; ?>" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-form-label">Judul</label>
				<div class="col-sm-6">
					<input type="text" name="judul" class="form-control" value="<?php echo $judul; ?>" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-form-label">Isi Halaman</label>
				<div class="col-sm-6">
					<textarea id="isi"  name="isi" class="form-control" rows="30"><?php echo $isi; ?></textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-form-label">Image</label>
				<div class="col-sm-6">
					<input type="file" name="image" value="<?php echo $image; ?>" required>
				</div>
			</div>
			<?php echo $button; ?>
		</form>
<?php
	}
	function insert($koneksi){
		$query = sprintf("INSERT INTO pages(name_page, judul, isi, image) VALUES ('%s', '%s', '%s', '%s')",$_POST['name_page'], $_POST['judul'], $_POST['isi'], $_POST['image']);
		$act = mysqli_query($koneksi, $query);
		if ($act) {
			header('location:?page=pages');
		} else {
			echo "<div class='alert alert-danger' role='alert'><span class='fa fa-close'></span> Gagal Insert Data!</div>";
		}
	}
	function delete($koneksi){
		$query = sprintf("delete from pages where id_page='%s'",$_GET['id']);
		$act = mysqli_query($koneksi,$query);
		if($act){
			header('location:?page=pages');
		}else{
			echo "gagal".mysqli_error($koneksi);
		}
	}
	function update($koneksi){		
			$query = sprintf("update pages set name_page='%s', judul = '%s', isi='%s', image='%s' where id_page ='%s'" ,$_POST['name_page'],$_POST['judul'],$_POST['isi'],$_POST['image'],$_POST['id']);
			$act = mysqli_query($koneksi,$query) or die(mysqli_error($koneksi));
			if ($act) {
				header('location:?page=pages');
			}else{
				echo "<br>gagal Update";
			}
		
	
}

	if (isset($_GET['add'])) {		
		form($koneksi);
	} else {
		ShowData($koneksi);
		if (isset($_GET['act']) && $_GET['act'] == 'hapus') delete($koneksi);
	}
	if (isset($_POST['insert'])) insert($koneksi);
	else if (isset($_POST['update'])) update($koneksi);
?>