<?php 
session_start();
include('contents/koneksi.php'); 
if(!isset($_SESSION['username'])){
  header('location:../login.php');
}else{

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Admin</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">

  <!-- TinyMce-->
  <script src="js/tinymce/tinymce.min.js"></script><script>
  tinymce.init({
    selector: '#isi'
  });
  </script>


</head>

<body id="page-top">

  <nav class="custom-nav navbar navbar-expand navbar-dark static-top">

    <a class="navbar-brand mr-1" href="index.php"p>WEBTECH ADMIN</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="../index.php">User Interface</a>
      </li>
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">          
          <a class="dropdown-item" href="../login.php?logout" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="cusid sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">
          <i class="fas fa-fw fa-home"></i>
          <span>Home</span>
        </a>
      </li>     
      <li class="nav-item">
        <a class="nav-link" href="?page=kategori">
          <i class="fas fa-fw fa-list-ul"></i>
          <span>Categories</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?page=artikel">
          <i class="far fa-fw fa-newspaper"></i>
          <span>Articles</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?page=pages">
          <i class="fas fa-fw fa-pager"></i>
          <span>Pages</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?page=komen">
          <i class="far fa-fw fa-comment-alt"></i>
          <span>Comments</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?page=galeri">
          <i class="far fa-fw fa-images"></i>
          <span>Gallery</span></a>
      </li>
    </ul>

    <div class="container-fluid">

      <div class=>
        <section class="content">
        <?php           
          if (isset($_GET['page'])) {
            include('contents/'.$_GET['page'].'.php');
          }
          else {
        ?>          
          <h3 class="pt-2">Welcome <?php echo $_SESSION['nama_user']; ?></h3>
          <hr>
          <h5>List Artikel Terbaru : </h5>
          <?php  
          $query = mysqli_query($koneksi, "SELECT * FROM artikel");  
          $i=1;         
          while ($data = mysqli_fetch_array($query)){
          ?>
          <p><?php echo $i++.". ".$data['judul']; ?></p>
        <?php } }?>
        </section>

      </div>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="../login.php?logout">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Page level plugin JavaScript-->
  <script src="vendor/chart.js/Chart.min.js"></script>
  <script src="vendor/datatables/jquery.dataTables.js"></script>
  <script src="vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin.min.js"></script>

  <!-- Demo scripts for this page-->
  <script src="js/demo/datatables-demo.js"></script>
  <script src="js/demo/chart-area-demo.js"></script>

</body>

</html>
<style>
.act{
  text-align: center;
}
.fa-trash{
    color: red;
}
th{
  text-align: center;
}
.custom-nav{
  background-color: #292929;
}
.cusid{
  background-color: #393939;
}
</style>
          <?php } ?>