<?php 
session_start();
include('contents/koneksi.php');
if (isset($_GET['logout'])) {
	unset($_SESSION['username']);
	unset($_SESSION['nama_user']);
	unset($_SESSION['id_user']);
	session_destroy();
	header('location:login.php');
}
if(isset($_POST['cancel'])){
	header('location:index.php');
}
if(!isset($_SESSION['username'])){
	if (isset($_POST['login'])) {
		$username = $_POST['username'];
		$password = $_POST['password'];
		$query = sprintf("SELECT * FROM users WHERE username='%s' and password='%s'",$username,$password);
		$res = mysqli_fetch_array(mysqli_query($con,$query));
		$nama_user = $res['nama_user'];
		$id_user = $res['id_user'];
		$jml = mysqli_num_rows(mysqli_query($con,$query));
		if ($jml==1) {
			$_SESSION['username'] = $username;
			$_SESSION['nama_user'] = $nama_user;
			$_SESSION['id_user'] = $id_user;
			header('location: admin/index.php');
		}else{
			echo "			
			<div class='alert-danger text-center rounded' role='alert'>Username atau Password Salah!</div>
			";
		}
	}
?>
<html>
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet" href="assets/css/bootstrap.css">
</head>
<body style="background:#292929">
<div class="col-lg-6 mx-auto">
	<div class="card">
	  <div class="card-header text-center">
	    <h3>Login</h3>
	  </div>
	  <div class="card-body">
	    <form method="post" action="login.php">
		  <div class="form-group">
		    <label for="username">Username</label>
		    <input type="text" name="username" class="form-control" id="username" placeholder="Enter Username">
		  </div>
		  <div class="form-group">
		    <label for="Password">Password</label>
		    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
		  </div>
		  <center>
			<button type="submit" name="login" class="btn btn-primary">Login</button>
			<button type="submit" name="cancel" class="btn btn-secondary">Cancel</button>
		  </center>
		</form>
	  </div>
	</div>
</div>
<?php 
}else{
	header('location:index.php');
}
?>
<script src="assets/js/jquery-slim.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>
<style>
	.card{
		margin-top: 10em;
	}
</style>