<?php
	include ('koneksi.php');
	$id = $_GET['isi']; 
	if (isset($_GET['isi'])) {
		$que = mysqli_query($con, "SELECT * FROM kategori JOIN artikel ON artikel.id_kategori = kategori.id_kategori JOIN users ON artikel.id_user = users.id_user WHERE artikel.id_artikel = '$id'");
		while ($res = mysqli_fetch_array($que)) { 
      ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $res['judul']; ?></title>
    <link rel="stylesheet" href="../assets/css/custom.css">
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">
    <link href="../admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<nav class="navbar navbar-custom navbar-expand-lg navbar-dark fixed-top">
  <div class="container">
    <a class="navbar-brand" href="index.php">WEBTECH</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav mx-auto">        
        <li class="nav-item active">
          <a class="nav-link" href="..">Home</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categories
          </a>
          <div class="dropdown-menu dropdown-menu-center" aria-labelledby="userDropdown">
            <?php 
            $kate = sprintf("SELECT * FROM kategori");
            $query = mysqli_query($con,$kate);
            while ($data = mysqli_fetch_array($query)){
            $nama = $data['nama_kategori'];
            $id = $data['id_kategori'];           
            ?>
            <a class="dropdown-item" href="../index.php?idkate=<?php echo $id; ?>"><?php echo $nama; ?></a>
            <?php } ?>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pages
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <?php 
            $kate = sprintf("SELECT * FROM pages");
            $query = mysqli_query($con,$kate);
            while ($data = mysqli_fetch_array($query)){
            $nama = $data['name_page'];
            $id = $data['id_page'];           
            ?>
            <a class="dropdown-item" href="../contents/detail.php?id=<?php echo $id; ?>"><?php echo $nama; ?></a>
            <?php } ?>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../?galeri">Gallery</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="../?video">Video</a>
        </li>        
      </ul>      
    </div>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="admin/index.php">Administrator</a>          
        </div>
      </li>
    </ul>
  </div>
  
</nav>

<section class="content">
  <!-- Full Page Image Header with Vertically Centered Content -->
  <header class="masthead pt-5">
    <div id="anakmast" class="h-100">
      <div class="container h-100">
        <div class="row h-100 align-items-center">
          <div class="col-12 text-center" id='tlsgbr'>
           <h1><?php echo $res['nama_kategori']; ?></h1>
          </div>
        </div>
      </div>
    </div>
  </header>  
  <!-- Page Content -->
  <div class="wrapper">
    <div class="container" id="theader">
      <section class="py-2">
        <center class="">
          <a href="../index.php">Home</a> /
          <a href="../index.php?idkate=<?php echo $res['id_kategori'] ;?>"><?php echo $res['nama_kategori']; ?></a> /
          <a><?php echo $res['judul']; ?></a>
        </center>      
      </section>
    </div>
    <div class="container">
      <div class="row" id="tisi">
        <div class="col">            
          <section class="py-5">        
          <h3><?php echo $res['judul']; ?></h3>
          <p>
            <span>Oleh : <?php echo $res['nama_user']; ?> - </span>
            <span><?php echo substr($res['created_at'],0,10); ?> ,</span>
            <span><?php echo substr($res['created_at'],11,5); ?> WIB </span>
          </p>
          <img src="../assets/images/<?php echo $res['image'] ?>" class="img-thumbnail" alt="">
          <p><?php echo $res['isi']; ?></p>
          </section>
        </div>
        <div class="col col-lg-4" >
          
        </div>
      </div>
      <div class="row">
        <div class="col">
          <section class="py-5">
            <h3>Comments</h3>
            <?php include('comment.php') ?>
          </section>
        </div>
        <div class="col col-lg-4" >
          
        </div>
      </div>
      
    </div>
  </div>
</section>
<!-- Footer -->
<footer class="page-footer text-center">
  <!-- Footer Elements -->
  <div class="container-fluid">
  <div class="pt-5">
      <h4 class="font-weight-light">Follow Us on:</h4>
    </div>
    <!-- Grid row-->
    <div class="row">      
      <!-- Grid column -->
      <div class="col-md-12 py-5">
        <div>
          <!-- Facebook -->
          <a class="ic" href="#">
            <i class="fab fa-facebook-f fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <!-- Twitter -->
          <a class="ic" href="#">
            <i class="fab fa-twitter fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <!-- Google +-->
          <a class="ic" href="#">
            <i class="fab fa-google-plus-g fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <!--Linkedin -->
          <a class="ic" href="#">
            <i class="fab fa-linkedin-in fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <!--Instagram-->
          <a class="ic" href="#">
            <i class="fab fa-instagram fa-lg white-text mr-md-5 mr-3 fa-2x"> </i>
          </a>
          <!--Pinterest-->
          <a class="ic" href="#">
            <i class="fab fa-pinterest fa-lg white-text fa-2x"> </i>
          </a>
        </div>
      </div>
      <!-- Grid column -->
    </div>
    <!-- Grid row-->
  </div>
  <!-- Footer Elements -->
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href="https://fb.com/iqbalmaulanaaz"> Iqbal Maulana</a>
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->

<script src="../assets/js/jquery-slim.min.js"></script> 
<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>
<?php  
$quejoin = mysqli_query($con, "SELECT a.id_artikel,a.id_kategori,k.id_kategori,k.image FROM artikel a INNER JOIN kategori k ON a.id_kategori=k.id_kategori WHERE a.id_artikel = '$id'");
while ($res = mysqli_fetch_array($quejoin)) {
?>
<style>
.masthead {
  height: 90vh;
  min-height: 500px;
  background-color: black;
  background-image: url('../assets/images/<?php echo $res['image']; ?>');
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  color: white;
}
#anakmast{
  background-color: rgba(0,0,0,75%);
}

#tlsgbr h1{
  font-weight: normal;
  font-size: 7em;
  text-transform: uppercase;
  padding: 50px;
  border: 10px solid white;
}
<?php } ?>
.img-thumbnail{
  width: 100%;
  max-height: 50vh;
  min-height: 300px;
  object-fit:  cover; 
  background-position: center;
  background-repeat: no-repeat;
}
.dropdown-toggle::after{
  display: none;
}
.navbar-custom{
  background-color: #292929;
  float: left;
}

footer .container-fluid{
  background: #393939;  
  color: #f8f9fb;
}
footer .page-footer{
  padding-top: 20px;
}
footer .footer-copyright{
  background: #292929; 
  color: #f8f9fb;
 
}
.wrapper{
  margin-top: 25px;
}
#theader{
  border: 1px solid black;
  border-radius: 25px;
  background-color: #393939;
  color: #f8f9fb;
}
#theader a{
  color: #f8f9fb;
}
#tisi h3{
  padding-bottom: 10px;
  border-bottom: 1px solid black;
}
.ic .fab{
  color: white;
  margin: 0 35px; 
}
.ic .fab:hover{
  color: blue;
}
</style>
<?php }}?>
