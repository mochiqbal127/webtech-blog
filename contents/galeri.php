<div class="wrapper">
  <div class="container">    
    <div class="py-5">
      <center class='pt-5'>
        <h1>Gallery</h1>
        <hr>
      </center>
     <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="" class="gbr d-block w-100" alt="slide show">
        </div>
      <?php 
        $kate = sprintf("SELECT * FROM galeri");
        $query = mysqli_query($con,$kate);
        while ($data = mysqli_fetch_array($query)){
        $image = $data['image'];     
      ?>
        <div class="carousel-item">
          <img src="assets/images/<?php echo $image; ?>" class="d-block w-100" alt="image">
        </div>
      <?php } ?>
      <hr>
      </div>
        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
    </div> 
    </div>
  </div>
</div>
<style>
  .gbr{
    font-size: 250px;
    text-transform: uppercase;
    text-align: center;
    background-color: lightgrey;    
    display: none;
  }
</style>
