<?php 
	include ('contents/koneksi.php');
?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>WEBTECH</title>
    <link rel="stylesheet" href="assets/css/custom.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link href="admin/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">


</head>
<body>
<nav class="navbar navbar-custom navbar-expand-lg navbar-dark fixed-top">
  <div class="container">
    <a class="navbar-brand" href="index.php">WEBTECH</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav mx-auto">        
        <li class="nav-item active">
          <a class="nav-link" href="?">Home</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Categories
          </a>
          <div class="dropdown-menu dropdown-menu-center" aria-labelledby="userDropdown">
            <?php 
            $kate = sprintf("SELECT * FROM kategori");
            $query = mysqli_query($con,$kate);
            while ($data = mysqli_fetch_array($query)){
            $nama = $data['nama_kategori'];
            $id = $data['id_kategori'];           
            ?>
            <a class="dropdown-item" href="index.php?idkate=<?php echo $id; ?>"><?php echo $nama; ?></a>
            <?php } ?>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Pages
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
            <?php 
            $kate = sprintf("SELECT * FROM pages");
            $query = mysqli_query($con,$kate);
            while ($data = mysqli_fetch_array($query)){
            $nama = $data['name_page'];
            $id = $data['id_page'];           
            ?>
            <a class="dropdown-item" href="contents/detail.php?id=<?php echo $id; ?>"><?php echo $nama; ?></a>
            <?php } ?>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="?galeri">Gallery</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="?video">Video</a>
        </li>        
      </ul>      
    </div>
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="admin/index.php">Administrator</a>          
        </div>
      </li>
    </ul>
  </div>
  
</nav>
<?php 
  if (isset($_GET['idkate'])) {?>
<section class="content-php">
  <div class="container py-5">
    <div class="row">
      <div class="col col-lg-8">            
        <section class="py-5">          
          <?php 
              include('contents/kategori.php');
            ?>
        </section>
      </div>
      <div class="col col-lg-4" >
        
      </div>
    </div>
  </div>
</section>
<?php }
elseif(isset($_GET['galeri'])){
  include('contents/galeri.php');
}
elseif(isset($_GET['video'])){
  include('contents/video.php');
}
else{ ?>
<section class="contents">
  <!-- Full Page Image Header with Vertically Centered Content -->
  <header class="masthead">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12 text-center" id='tlsgbr'>
          <h1 class="font-weight-light">WELCOME TO WEBTECH</h1>
          <p class="lead">WEBSITE OF MAGAZINE TECHNOLOGY</p>
        </div>
      </div>
    </div>
  </header>
  <!-- Page Content -->
  <div class="wrapper">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">            
          <section class="py-5">        
          <h3 id="atasan">Artikel Terkini</h3>       
            <div class="container">
              <?php
              $que = mysqli_query($con, "SELECT * FROM artikel order by created_at desc");
              while ($res = mysqli_fetch_array($que)) { ?>
              <div class="row rounded" id='kotakan'>             
                <div class="col-lg-3">
                  <img src="assets/images/<?php echo $res['image'];?>" class="rounded"  width="150px" height="150px">
                </div>
                <div class="col">
                  <a href="contents/artikel.php?isi=<?php echo $res['id_artikel'] ?>">              
                    <h5><?php echo $res['judul']; ?></h5>
                  </a>
                    <small class="substr"><?php echo substr($res['isi'],0,250)."..."; ?></small>
                    <hr>
                    <p>
                      <span class="float-left text-muted"><?php echo substr($res['created_at'],0,10); ?></span>
                        <a href="contents/artikel.php?isi=<?php echo $res['id_artikel'] ?>">
                      <span class="rdm float-right rounded" >Read More ></span>                    
                      </a><br>
                      <small class="float-left text-muted">pada jam : <?php echo substr($res['created_at'],11,5) ?></small>
                    </p>                                
                </div>
              </div>
              <?php } ?>
            </div>
          </section>
        </div>
        <div class="col col-lg-4" >
          
        </div>
      </div>
    </div>
  </div>

</section>
<?php } ?>
<!-- Footer -->
<footer class="page-footer text-center">
  <!-- Footer Elements -->
  <div class="container-fluid">
  <div class="pt-5">
      <h4 class="font-weight-light">Follow Us on:</h4>
    </div>
    <!-- Grid row-->
    <div class="row">      
      <!-- Grid column -->
      <div class="col-md-12 py-5">
        <div>
          <!-- Facebook -->
          <a class="ic" href="#fb">
            <i class="fab fa-facebook-f fa-lg fa-2x"> </i>
          </a>
          <!-- Twitter -->
          <a class="ic" href="#tw">
            <i class="fab fa-twitter fa-lg fa-2x"> </i>
          </a>
          <!-- Google +-->
          <a class="ic" href="#gl">
            <i class="fab fa-google-plus-g fa-lg fa-2x"> </i>
          </a>
          <!--Linkedin -->
          <a class="ic" href="#in">
            <i class="fab fa-linkedin-in fa-lg fa-2x"> </i>
          </a>
          <!--Instagram-->
          <a class="ic" href="#ig">
            <i class="fab fa-instagram fa-lg fa-2x"> </i>
          </a>
          <!--Pinterest-->
          <a class="ic" href="#pn">
            <i class="fab fa-pinterest fa-lg fa-2x"> </i>
          </a>
        </div>
      </div>
      <!-- Grid column -->
    </div>
    <!-- Grid row-->
  </div>
  <!-- Footer Elements -->
  <!-- Copyright -->
  <div class="footer-copyright text-center py-3">© 2019 Copyright:
    <a href="https://fb.com/iqbalmaulanaaz"> Iqbal Maulana</a>
  </div>
  <!-- Copyright -->
</footer>
<!-- Footer -->


<script src="assets/js/jquery-slim.min.js"></script> 
<script src="assets/js/bootstrap.min.js"></script>
</body>
</html>
<style>
.masthead {
    height: 100vh;
    min-height: 500px;
    background-image: url('assets/images/techwp.jpg');
    background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    color: white;
  }
.navbar-custom{
  background-color: #292929;
}
#tlsgbr{
  background-color: #092e41;
}
#kotakan{
  border: 1px solid #afafaf;
  margin-bottom : 20px;
  padding-bottom : 12.5px;
  background-color : #f8f9fb;
}
#kotakan img{
  width: 150px;
  height: 150px;
  object-fit: cover;
  margin-left : 10px;
  margin-top : 27px;
}
#kotakan a{
  color : #181818;
  
}
#kotakan h5{
  padding: 10px 0;
  border-bottom: 1px solid grey;
}
#kotakan a:hover{
  text-decoration: none;
}
#kotakan h5:hover{
  color : blue;
}
#kotakan .rounded{
  border : 1px solid #afafaf;
  padding: 7px;
}
#kotakan .rdm{
  margin-top: 5px;
  font-weight: normal;
}
#kotakan .rdm:hover{
  color: white;
  background-color: #292929;
}
#atasan{
  color : #181818;
  padding-bottom : 5px;
  border-bottom : 1px solid #181818;
  margin-bottom : 20px;
  margin-left :-30px;
}
footer .container-fluid{
  background: #393939;  
  color: #f8f9fb;
}
footer .page-footer{
  padding-top: 20px;
}
footer .footer-copyright{
  background: #292929; 
  color: #f8f9fb;
}
.substr strong{
  font-weight: normal;
}
.substr span{
  font-weight: normal;
  color: black;
}

.ic .fab{
  color: white;
  margin: 0 20px; 
}
.ic .fab:hover{
  color: blue;  
}
.dropdown-toggle::after{
  display: none;
}
</style>
